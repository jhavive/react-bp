import React, { useState } from 'react'
import './styles.scss'

const RATINGS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

const View = props => {

    let [value, setValue] = useState(props.value)

    const onChange = event => {
        event.stopPropagation()
        let index   =   event.target.getAttribute('data-type')
        if(index){
            setValue(index)
            props.onChange(props.name, index)
        }
    }

    return  <div className="nps-scale-view" onClick={onChange}>
        <div className="flex flex-end">
            <h5>{props.label}</h5>
            <p><u>Scan QR Code</u></p>
        </div>
        <input required value={value}/>
    </div>
}

export default View 