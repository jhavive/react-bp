import React from 'react'
import Loadable from 'react-loadable'

export const LoadableLoading = () => {
    let { dispatch } = window.rootStore
    // console.log("CLOSE_LOADER")
    dispatch({
        type: "OPEN_LOADER"
    })
    return <div></div>
}

export const LoadableRender = (loaded, props) => { 
    let Component = loaded.default
    let { dispatch } = window.rootStore
    console.log("CLOSE_LOADER")
    dispatch({
        type: "CLOSE_LOADER"
    })
    return <Component {...props} /> 
}

export const goBack = (steps = -1) => {
    let arr = location.pathname.split('/')
    arr.pop()
    return arr.join('/')
}