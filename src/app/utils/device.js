export const isMobile = () => {
    return window.matchMedia("(min-width: 320px) and (max-width: 767px) and (orientation: portrait)").matches
}

export const isMediumPortrait = () => {
    return window.matchMedia("(min-width: 768px) and (orientation: portrait)").matches
}

export const isLargePortrait = () => {
    return window.matchMedia("(min-width: 768px) and (orientation: portrait)").matches
}

export const isXLPortrait = () => {
    return window.matchMedia("(min-width: 768px) and (orientation: portrait)").matches
}

export const isSmallLandscape = () => {
    return window.matchMedia("(min-width: 600px) and (max-width: 767px) and (orientation: landscape)").matches
}

export const isMediumLandscape = () => {
    return window.matchMedia("(min-width: 768px) and (max-width: 1367px) and (orientation: landscape)").matches
}

export const isLargeLandscape = () => {
    return window.matchMedia("(min-width: 1367px) and (max-width: 1920px) and (orientation: landscape)").matches
}