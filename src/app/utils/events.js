export const LongPress = (node, callback) => {
    let longpress = false
    let presstimer = null
    
    const cancel = e => {
        if (presstimer !== null) {
            clearTimeout(presstimer)
            presstimer = null
        }
    }

    const click = e => {
        if(presstimer != null){
            clearTimeout(presstimer)
            presstimer = null
        }
        if(longpress)
            return false
    }
    
    const start = e => {    
        if (e.type === "click") {
            return
        }
    
        longpress = false;
    
        if (presstimer === null) {
            presstimer = setTimeout(() => {
                callback()
                longpress = true;
            }, 500);
        }
    
        return false;
    }
    
    node.addEventListener("mousedown", start)
    node.addEventListener("touchstart", start)
    node.addEventListener("click", click)
    node.addEventListener("mouseout", cancel)
    node.addEventListener("touchend", cancel)
    node.addEventListener("touchleave", cancel)
    node.addEventListener("touchcancel", cancel)
} 

export const debounce = function(func, delay) {
    let debounceTimer
    console.log(this)
    return function(){
        clearTimeout(debounceTimer)
        debounceTimer = setTimeout(func, delay)
    }.bind(this)
}