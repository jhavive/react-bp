const baseURL = {
    // development: 'https://aea783cb2d8e.ngrok.io',
    development: 'http://localhost:8000',
    staging: 'https://api.staging.bristag.com',
    production: "https://api.bristag.com",
}

const config = {
    baseURL: baseURL[process.env.SERVER_ENV ? process.env.SERVER_ENV : 'development'],
    getRoutes: {
        path: 'get-routes',
        methods: ['GET']
    },


} 

export default config

