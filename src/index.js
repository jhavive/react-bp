/* eslint-disable*/

import React from 'react'
import { render } from 'react-dom'
import Root from './app/pages'
import './app/assets/scss/reset.scss';

render(<Root />, document.getElementById('index'))